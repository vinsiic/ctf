# LOG

## Description

I recently found a file in my personal folder which is not mine. I dont know what it is but I hope that none messed with my PC.

## Challange download and instructions

[forensics_log](forensics_log.tar.gz)

- unzip file

## Solution

[https://bitvijays.github.io/LFC-Forensics.html](https://bitvijays.github.io/LFC-Forensics.html)

<details>
<summary>Click this to collapse/fold.</summary>

```bash
tshark -r log.pcap -T fields -e usbhid.data usb.device_address==3 and usbhid.data | sed 's/../:&/g2'
```

[https://github.com/TeamRocketIst/ctf-usb-keyboard-parser](https://github.com/TeamRocketIst/ctf-usb-keyboard-parser)

</details>

## Todo

Complete writeup :dizzy_face:
