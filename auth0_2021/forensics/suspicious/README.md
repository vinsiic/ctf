# Suspicious

## CHALLENGE INFO

We have noticed that one of our IT employees has access to servers and files that he should not. We confronted him but he denied every accusation. Can you prove him wrong?

## Challange download and instructions

[forensics_suspicious](forensics_suspicious.zip)

- unzip file

## Solution

[https://systemoverlord.com/2021/03/12/bsidessf-2021-ctf-net-matroyshka-author-writeup.html](https://systemoverlord.com/2021/03/12/bsidessf-2021-ctf-net-matroyshka-author-writeup.html)

<details>
<summary>Click this to collapse/fold.</summary>

Wireshark FTP-DATA -> Follow / TCP Stream > Show data as RAW > Save as ...

</details>

## Todo

Complete writeup :dizzy_face:
