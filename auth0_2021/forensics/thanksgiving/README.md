# Thanksgiving

## CHALLENGE INFO

I recently received an email from ou HR that promts us to fill the dates we want to leave from work during thanksgiving. The problem is that I am not able to edit the file even if I enable editing like the document suggests.

## Challange download and instructions

[forensics_thanksgiving](forensics_thanksgiving.zip)

- unzip file

## Solution

[https://ilovectf.github.io/re/vba/2020/03/29/Volga.html](https://ilovectf.github.io/re/vba/2020/03/29/Volga.html)\
[https://pypi.org/project/pcode2code/](https://pypi.org/project/pcode2code/)

<details>
<summary>Click this to collapse/fold.</summary>

```bash
pcode2code vbaProject.bin -o decompile.vba
```

```bash
python3 solve.py
```

</details>

## Todo

Complete writeup :dizzy_face:
