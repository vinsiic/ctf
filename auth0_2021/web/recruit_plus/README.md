# Recruit Plus

## CHALLENGE INFO

You've been tasked with a pentesting engagement on a recruitment portal system, they've provided you with a mockup build of the website and they've asked you to find a way to login as "admin".

## Challange download and instructions

[web_recruit_plus](web_recruit_plus.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

This task was simplest one if I understood point of it at the beginning :)

<details>
<summary>Click this to collapse/fold.</summary>

To solve the task you need remake JWT token with admin user name and in this case that was simple because you are provided with code and signing private/public keys. I was just so focused on other parts, but when I read code more carefully, I noticed, that I don't need to do anything complicated.

![api login source code](00_source-code_login.PNG)

We can compare if in production it uses same KID, so let's just use jwt.io and look inside header:

```
{
  "alg": "RS256",
  "typ": "JWT",
  "kid": "ff9738bf-1817-48b6-ad8d-578bff9cb6bf.pub"
}
```

It looks like key is same and also we can verify / sign with private/public keys from source.

This is problematic code where you can abuse JWT and get flag:

![dashboard source code](00_source-code_dashboard.PNG)

So, first thing is to register user, then use provided private/public keys under `private` folder and change username to admin, then request `/dashboard` and thats all folks ;)

![jwt.io](01_jwt-io.PNG)

Main take-away? Don't overcomplicate things ;)

</details>

## Flag

HTB{1_h4v3_r5a_Tru5t_i55u3s}
