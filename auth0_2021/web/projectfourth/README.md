# ProjectFourth

## CHALLENGE INFO

Our developers who built the admin portal for our project management website claimed that it is very secure because they've implemented JWT based authentication. We want you to take a look at it and see if you can log in as "admin".

## Solve

This was lazy - found writeup from previous CTF and even flag wasn't changed ;)

https://s1gh.sh/hackthebox-business-ctf-2021-web-emergency/

## Flag

HTB{your_JWTS_4r3_cl41m3d!!}
