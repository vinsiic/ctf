# PageOneHTML

## CHALLENGE INFO

Our employees needed a Markdown to HTML converter that will also directly embed all media in the HTML for offline portability. Since this is a public-facing application, we want you to take a look at it and make sure it is secure.

## Challange download and instructions

[web_pageonehtml](web_pageonehtml.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

<details>
<summary>Click this to collapse/fold.</summary>

**NOT MY SOLUTION** - I never solved this. Solution is from HTB discord by **st4ckh0und**

PageOneHTML (Web)
We have a website that can convert MarkDown to HTML, and which will embed images (and other data) as Base64 into the HTML website if transferred. We notice that the website has a /api/dev endpoint which transmits the flag if the request origins from 127.0.0.1 (localhost) and the request contains a specific key in the x-api-key header. We can easily just reference the link in the source-code as a MarkDown image, but it will fail to retrieve the flag, as we do not transfer the necessary header.

However, using the GOPHER protocol, we are able to write raw HTTP queries to the same socket, effectively setting the x-api-key header, while still just referencing the link, making the website request the data itself (hence the request being from localhost). We can thus retrieve the Base64 flag using the following MarkDown:

`gopher://127.0.0.1:1337/_GET%20/api/dev%20HTTP/1.0%0AHost:%20127.0.0.1:30458%0Ax-api-key:%20934caf984a4ca94817ea6d87d37af4b3%0A%0A`

</details>

## Flag
