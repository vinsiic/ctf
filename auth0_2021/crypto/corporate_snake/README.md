# Corporate Snake

## CHALLENGE INFO

The building is on lockdown! We are on DEFCON 1! Our security engineers have captured an encrypted message which was sent to our rival company. The message was traced back to one of our company's computers and we managed to get access to the encryption algorithm which was used to encrypt it. We need to decipher the message in order to find out what information was leaked to our competitor. Unfortunately, none of our engineers knows anything about cryptography, so you are our only hope. We provided you with the source code of the encryption algorithm and the ciphertext. Don't let the Big Boss down and you might get a Christmas bonus this year!

This challenge has a downloadable part.

## Challange download and instructions

[crypto_corporate_snake](crypto_corporate_snake.zip)

- unzip file

## Todo

I didn't solve this and will be updating with writeup from HTB discord.
