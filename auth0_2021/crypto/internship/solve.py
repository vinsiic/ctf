import jwt

with open("public_key.pem", "r") as f:
    public = f.read()

with open("private_key.pem", "r") as f:
    private = f.read()

user = "sn"
logged_in = True

print(public)
print("")
print(private)
print("")

print(jwt.encode({"user": user, "logged_in" : logged_in}, private, algorithm='RS256'))