# Internship

## CHALLENGE INFO

Here in 1337 Industries, we take pride in producing high-quality, secure web applications. Our new intern coded a demo login app to show off his web security skills, which he arrogantly claims is unpenetrable. However, something seems off, and since you are our best engineer can you investigate this?

This challenge is started on-demand.\
This challenge has a downloadable part.

## Challange download and instructions

[crypto_internship](crypto_internship.zip)

- unzip file

## Solve

<details>
<summary>Click this to collapse/fold.</summary>

Let's use [JWT.io](https://jwt.io/) to decode JWT token:

```
eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsInB1YmxpY19rZXkiOnsibiI6InBOaVFBNVVWeEczQzA5OVJPbHdSSVI3VFV5Q0tkTXlvbVNUQWk5Y1puZ3B2ZGVWQ2JsNVluRjR2V21fN2tsTXhkSlhhbkFWSEIxbXBIQW5JZkRDQVpJV0NXbUkzQzNGOEpVUEt2dFI2bTdnRmQyc1Y1WTl0UVdiTWlYTGdHY3Q0Tm9fcGRpcWRRdnA1MFg0alF3MExXSXJIaHdOMno5NjVUZnNLRE5jVC1vUU4yY0E5T3Z1RWdzRGpKRW9aSy1GTzlNZ2tiNDRBdlBwSDJSZjZ0SE9IOVBnQUJsZldMbFNhQjRRV2tJbmNuaGMxQW4wTWgxWE5BNHZLWHotd0YxTWo3MGlYOURubjBVWm1GdzhkSHlFTkNTVVFObDJieTI1RUdoNFJMRzJHSlJoUjZhWlNvZEtUWEZHNG4tcDZTYU9DMm1MM2MtSmxyemx6Ml9Cckk4NUV6dyIsImUiOiJBUUFCIiwia3R5IjoiUlNBIn19.eyJ1c2VyIjoiZ3Vlc3QiLCJsb2dnZWRfaW4iOmZhbHNlfQ.SqLrOaRPMEt2eGaHi2rb4TbDhOCZC-iFEhtuRMpV6BGvkGErCU469mf2_6UVRK0lgm4P670iHgRjhUPDZpDFTZU6vcrI7vpf4eAsueqAGNcYi6zLwYvhvFyMZtHIFClFFMDBf5m0-3FxpjQ0ZeePFXUSyHSXGIR4n_YFE6-YfionjAUimNUniqkoFsf6p_-TuOVMx8tu5NaqB3qQEMdkD4kuGGyaiXPl_fwftNzjYaKCm6u5HWV1cd7b77xtSnQn2oE37jPBKp1wnK5ojuvrwhEBjr9Y9FS_3siORvX4DGixzYgbJ_cD6NhuREGtKDlTqGREgeP6-zU3IGfKXwtx7Q
```

```
## HEADER ##
{
  "typ": "JWT",
  "alg": "RS256",
  "public_key": {
    "n": "pNiQA5UVxG3C099ROlwRIR7TUyCKdMyomSTAi9cZngpvdeVCbl5YnF4vWm_7klMxdJXanAVHB1mpHAnIfDCAZIWCWmI3C3F8JUPKvtR6m7gFd2sV5Y9tQWbMiXLgGct4No_pdiqdQvp50X4jQw0LWIrHhwN2z965TfsKDNcT-oQN2cA9OvuEgsDjJEoZK-FO9Mgkb44AvPpH2Rf6tHOH9PgABlfWLlSaB4QWkIncnhc1An0Mh1XNA4vKXz-wF1Mj70iX9Dnn0UZmFw8dHyENCSUQNl2by25EGh4RLG2GJRhR6aZSodKTXFG4n-p6SaOC2mL3c-Jlrzlz2_BrI85Ezw",
    "e": "AQAB",
    "kty": "RSA"
  }
}

## PAYLOAD ##
{
  "user": "guest",
  "logged_in": false
}
```

We have JWK, that we could use for PRIVATE KEY restore by using RsaCtfTool, but for now, let's convert JWK to PEM with [JWK to PEM converter online](https://8gwifi.org/jwkconvertfunctions.jsp)

```
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApNiQA5UVxG3C099ROlwR
IR7TUyCKdMyomSTAi9cZngpvdeVCbl5YnF4vWm/7klMxdJXanAVHB1mpHAnIfDCA
ZIWCWmI3C3F8JUPKvtR6m7gFd2sV5Y9tQWbMiXLgGct4No/pdiqdQvp50X4jQw0L
WIrHhwN2z965TfsKDNcT+oQN2cA9OvuEgsDjJEoZK+FO9Mgkb44AvPpH2Rf6tHOH
9PgABlfWLlSaB4QWkIncnhc1An0Mh1XNA4vKXz+wF1Mj70iX9Dnn0UZmFw8dHyEN
CSUQNl2by25EGh4RLG2GJRhR6aZSodKTXFG4n+p6SaOC2mL3c+Jlrzlz2/BrI85E
zwIDAQAB
-----END PUBLIC KEY-----
```

Let's restore PRIVATE KEY by using [RsaCtfTool github](https://github.com/Ganapati/RsaCtfTool)

```bash
# python RsaCtfTool.py --publickey /mnt/ctf/auth0_2021/crypto/internship/public_key.pem --private

[*] Testing key /mnt/ctf/auth0_2021/crypto/internship/public_key.pem.
[*] Performing factordb attack on /mnt/ctf/auth0_2021/crypto/internship/public_key.pem.
[*] Attack success with factordb method !

Results for /mnt/ctf/auth0_2021/crypto/internship/public_key.pem:

Private key :
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEApNiQA5UVxG3C099ROlwRIR7TUyCKdMyomSTAi9cZngpvdeVC
bl5YnF4vWm/7klMxdJXanAVHB1mpHAnIfDCAZIWCWmI3C3F8JUPKvtR6m7gFd2sV
5Y9tQWbMiXLgGct4No/pdiqdQvp50X4jQw0LWIrHhwN2z965TfsKDNcT+oQN2cA9
OvuEgsDjJEoZK+FO9Mgkb44AvPpH2Rf6tHOH9PgABlfWLlSaB4QWkIncnhc1An0M
h1XNA4vKXz+wF1Mj70iX9Dnn0UZmFw8dHyENCSUQNl2by25EGh4RLG2GJRhR6aZS
odKTXFG4n+p6SaOC2mL3c+Jlrzlz2/BrI85EzwIDAQABAoIBAFEb1VBN7JXnw7Ln
8Fmcrzjyp9wA6N8rS9F+SdcEkTA3xjvl/9e6SDM4DGidLNUEZhMNILEfz6EINrL7
BVBFtQbAwjCinp7cyc60Gm/msCUqWFY3tGBtP76sS/tdMHLAsHd4O9DOiu5Hu+Se
Ac+d/XUlY6NEyxLgbaC24m0K7mOWB+rbagi+nntG3G6QeZHmbb121ZuG+6TxJR2v
K2IwqKOoNzQGiSAVdXYwEt+UsHu0NHZhZsMbEjEdViU0dXMhTj/YUb7ZHx6NG7CT
4M91b+N2o61oBh4SHPHOl/GVBEv32YMM0wkDsXYQPCZJBb2Q+z6uE9lByIgUoyVW
iSXdcfECgYEAzW2BW0Su75RRWEmcVu7C4Sm+nGDZ6lSwYtpZflr0s37zW+m2Wn7e
ouZcoKGt/rWI4TsWjh80XdAh7T3vmqm/gg2XPSJqcfcJss7gpHmYsHa7kEyMbCkJ
crolXeoDjFZYgNA3iI99xLo0SK9lRgJim1YyYNj+Mg6LlMiBv/4BZJUCgYEAzW2B
W0Su75RRWEmcVu7C4Sm+nGDZ6lSwYtpZflr0s37zW+m2Wn7eouZcoKGt/rWI4TsW
jh80XdAh7T3vmqm/gg2XPSJqcfcJss7gpHmYsHa7kEyMbCkJcrolXeoDjFZYgNA3
iI99xLo0SK9lRgJim1YyYNj+Mg6LlMiBv/4BZtMCgYByVcx1e9jRx38u4Gl/iQw2
PgG1D8K5xcEWg+jjhsRDUxknwwCXhZ1slUJgtRwec/NnSHfZSkzfGgSIArzlHC1W
uTG/+PO90n03ZCVr5bGyJaNnQPO3D3AOxsQdbS3kYfqIV1tRSmv5npmTCuwW9yTi
yCrp9FwmiCnv+Vkd/sXPsQKBgBtKCr8XFq4Y4914md4qhZTnuLyJWSl56pyXKQBS
EOOvX6C8IU9QPz8gnvlvwO9vKinRcaObnZdAyOD9M1E2b4xVRdwHMGDHUnCAbvMq
fBAlFbGLmbPcl7a357LcHXCNH+CsyFPJzAlQOxXUxv1P382G29ZlvQ7lQ8GWCTZ6
BbGlAoGBAKiQtmjFvQlFhaWoVQY34CIDevUq5owZyO4ao4Z0js/xFxOrtGexPyjm
8s67+HkkkcRpIFVmUOEMdQmVYu+/BfOiyiYfXLJA5Z3fRihpIPhE4ihbWyQf46CX
2fqNn+aqwQpBmX+qXrn0qnS6vM0SA09LoqX9377tm5DUsdOBfDyu
-----END RSA PRIVATE KEY-----
```

Small python script to create JWT with `logged_in = True`

```python
import jwt

with open("public_key.pem", "r") as f:
    public = f.read()

with open("private_key.pem", "r") as f:
    private = f.read()

user = "sn"
logged_in = True

print(public)
print("")
print(private)
print("")

print(jwt.encode({"user": user, "logged_in" : logged_in}, private, algorithm='RS256'))
```

Request (this is from Burp suite)

```
GET /flag HTTP/1.1
Host: 165.22.121.146:32699
User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Cookie: ACCESS_TOKEN=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ1c2VyIjoic24iLCJsb2dnZWRfaW4iOnRydWV9.GPu3l2KzOV2K-FwPbWdNhRXim4Q20YQogB34qWuHA4RPtrfxDQZDWmRi53yk-MZpU4D7EQC2KahpHJ4aTFEiXLiH8GOSJyL-OdMG7HybUbhC_jYBuK2JVmu2dRvf5ZZxGjsxj8afoWVDtd1yIoD9VHOTqJV_OrDJ05IqaoVkWKUGuOtd0pqPq7fJbMATnTcgN4SuceUnnOMUomjAhCVPvCoikcD9zxEciSSdPoXD2C___OOE15VI-3F-bvy6V3Dhtzhi8E0V4B0LWjqt7ttfysyx9cvXpNHN4_spz_GTo8g1rqtp8EH-l-a1tPJwAEwVGjolIpHjukaFZ3ijasB6eg
```

</details>

## Flag

HTB{f3rmats_fact0r1zation_is_pr3tty_c00l}
