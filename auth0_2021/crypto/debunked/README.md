# Debunked

## CHALLENGE INFO

You're a hard-working employee of MoonX, but you can't just look the other way anymore. They're hiding something here. It could be aliens, robots, or even worse... alien robots! And the only way to find out is to pass through the Permission Level 5 Gate Door. Use your hacking skills to find what's in there.

## Challange download and instructions

[crypto_debunked](crypto_debunked.zip)

- unzip file

## Todo

I didn't solve this and will be updating with writeup from HTB discord.
