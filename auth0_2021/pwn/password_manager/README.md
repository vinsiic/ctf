# Password Manager

## CHALLENGE INFO

Tired of forgetting your passwords? Hackers easily crack your weak passwords? With this new password manager, you can create and save up to 3 passwords for any application you want! For new users, it generates a super strong main password that you can change later.

This challenge is started on-demand.\
This challenge has a downloadable part.

## Todo

Update with writeup.